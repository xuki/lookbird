//
//  LBPhotoViewController.m
//  LookBird
//
//  Created by Jason Dinh on 13/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "LBPhotoViewController.h"
#import "Masonry.h"
#import "UIImageView+WebCache.h"

@interface LBPhotoViewController ()

@property (strong, nonatomic) UIImageView *imageView;
@property (copy) NSURL *url;

@end

@implementation LBPhotoViewController

- (instancetype) initWithPhotoURL: (NSURL *) photoURL {
    self = [super init];
    if (self) {
        self.url = photoURL;
    }
    return self;
}

- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = @"Photo";
    self.view.backgroundColor = [UIColor blackColor];
    [self.view addSubview: self.imageView];
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.imageView sd_setImageWithURL: self.url];
    
    //Allow tap to dismiss vc
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(tapped:)];
    [self.imageView addGestureRecognizer: tap];
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void) tapped: (UITapGestureRecognizer *) gesture {
    [self dismissViewControllerAnimated: YES completion: nil];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

#pragma mark lazy inits

- (UIImageView *) imageView {
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.userInteractionEnabled = YES;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return _imageView;
}

@end
