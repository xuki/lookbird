//
//  SearchViewController.m
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "SearchViewController.h"
#import "UIViewController+TinyWhale.h"
#import "Masonry.h"
#import "TweetCell.h"
#import "TwitterAPI.h"
#import "LBTweet.h"
#import "LoadingCell.h"
#import "LBPhotoViewController.h"
#import "LBTweetMedia.h"
#import "PhotoAnimator.h"
#import "AdvanceSearchCell.h"
#import "AdvanceSearchDatePickerCell.h"
#import "AdvanceSearchMapViewController.h"
@import SafariServices;
#import "EmptyCell.h"

@interface SearchViewController () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, TweetCellDelegate, UIViewControllerTransitioningDelegate, AdvanceSearchDatePickerCellDelegate, AdvanceSearchMapViewControllerDelegate>

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UISearchBar *searchBar;
@property (strong, nonatomic) TwitterAPI *twitterAPI;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (copy) NSArray<LBTweet *> *tweets;
@property (copy) NSString *currentKeyword;
@property (weak, nonatomic) LBTweetMedia *selectedTweetMedia;
@property (weak, nonatomic) UIImageView *tappedMediaView;
@property (strong, nonatomic) PhotoAnimator *animator;

//tableview variable
@property NSInteger tweetResultsSection;
@property NSInteger advanceSearchSection;
@property BOOL advanceSearchEnabled;
@property NSInteger advanceLocationIndex;
@property NSInteger advanceUntilIndex;
@property CLLocation *advanceLocation;
@property (copy) NSDate *advanceUntilDate;
@property (strong, nonatomic) AdvanceSearchMapViewController *mapViewController;
@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.advanceSearchEnabled = NO;
    self.tweetResultsSection = 0;
    self.advanceSearchSection = -1;
    self.advanceLocationIndex = 0;
    self.advanceUntilIndex = 1;
    
    self.title = @"Search";
    
    [self.view addSubview: self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    self.tableView.tableHeaderView = self.searchBar;
    
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.width.equalTo(self.tableView);
    }];
    
    [self.tableView addSubview: self.refreshControl];
}

- (void) refreshed: (UIRefreshControl *) sender {
    if (self.currentKeyword) {
        [self fetchTwitterResults];
    }
    else {
        [sender endRefreshing];
    }
}

- (void) showAdvanceSearch: (UIBarButtonItem *) sender {
    UIBarButtonItem *advanceButton = [[UIBarButtonItem alloc] initWithTitle: @"Simple" style:UIBarButtonItemStylePlain target: self action: @selector(hideAdvanceSearch:)];
    self.navigationItem.rightBarButtonItem = advanceButton;
    
    self.advanceSearchSection = 0;
    self.tweetResultsSection = 1;
    self.advanceSearchEnabled = YES;
    
    [self.tableView beginUpdates];
    [self.tableView insertSections: [NSIndexSet indexSetWithIndex: 0] withRowAnimation: UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void) hideAdvanceSearch: (UIBarButtonItem *) sender {
    UIBarButtonItem *advanceButton = [[UIBarButtonItem alloc] initWithTitle: @"Advance" style:UIBarButtonItemStylePlain target: self action: @selector(showAdvanceSearch:)];
    
    self.navigationItem.rightBarButtonItem = advanceButton;
    
    self.advanceSearchSection = -1;
    self.tweetResultsSection = 0;
    self.advanceSearchEnabled = NO;
    
    [self.tableView beginUpdates];
    [self.tableView deleteSections: [NSIndexSet indexSetWithIndex: 0] withRowAnimation: UITableViewRowAnimationFade];
    [self.tableView endUpdates];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) fetchTwitterResults {
    
    self.twitterAPI = [[TwitterAPI alloc] init];
    NSDictionary *advanceParams = [self advanceParams];
    [self.twitterAPI searchTwitterWithKeyword: self.currentKeyword advanceParams: advanceParams completion:^(NSArray<LBTweet *> *tweets, NSError *error) {
        if (tweets) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.refreshControl endRefreshing];
                self.tweets = tweets;
                [self.tableView reloadData];
                [self.tableView flashScrollIndicators];
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: @"Error" message: @"Could not load results from Twitter. Please try again" preferredStyle: UIAlertControllerStyleAlert];
                [alertController addAction:  [UIAlertAction actionWithTitle: @"Dismiss" style: UIAlertActionStyleDefault handler: nil]];
                [self presentViewController: alertController animated: YES completion: nil];
                [self.tableView reloadData];
            });
        }
    }];
    self.tweets = @[];
    [self.tableView reloadData];
}

//run when reach the end of tableview, try to load more result. Reuse the same twitterAPI
- (void) fetchMoreTwitterResults {
    
    NSDictionary *advanceParams = [self advanceParams];
    [self.twitterAPI searchTwitterWithKeyword: self.currentKeyword advanceParams: advanceParams completion:^(NSArray<LBTweet *> *tweets, NSError *error) {
        if (tweets) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                NSMutableArray *indexPaths = [NSMutableArray array];
                for (NSInteger i = 0; i < tweets.count; ++i) {
                    [indexPaths addObject: [NSIndexPath indexPathForRow:self.tweets.count + i inSection:0]];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    //TODO figure out how to insert cell without jumping
                    self.tweets = [self.tweets arrayByAddingObjectsFromArray: tweets];
                    [self.tableView reloadData];
                    [self.tableView flashScrollIndicators];
                });
            });
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alertController = [UIAlertController alertControllerWithTitle: @"Error" message: @"Could not load results from Twitter. Please try again" preferredStyle: UIAlertControllerStyleAlert];
                [alertController addAction:  [UIAlertAction actionWithTitle: @"Dismiss" style: UIAlertActionStyleDefault handler: nil]];
                [self presentViewController: alertController animated: YES completion: nil];
                [self.tableView reloadData];
            });
        }
    }];
}

- (NSDictionary *) advanceParams {
    if (self.advanceSearchEnabled) {
        NSMutableDictionary *mutableParams = [NSMutableDictionary dictionary];
        if (self.advanceLocation) {
            mutableParams[@"geocode"] = [NSString stringWithFormat: @"%f,%f,10km", self.advanceLocation.coordinate.latitude, self.advanceLocation.coordinate.longitude];
        }
        if (self.advanceUntilDate) {
            mutableParams[@"until"] = [self stringFromDate: self.advanceUntilDate];
        }
        return mutableParams;
    }
    return nil;
}

#pragma mark UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    [self.searchBar setShowsCancelButton: YES animated: YES];
    UIBarButtonItem *advanceButton;
    if (!self.advanceSearchEnabled) {
        advanceButton = [[UIBarButtonItem alloc] initWithTitle: @"Advance" style:UIBarButtonItemStylePlain target: self action: @selector(showAdvanceSearch:)];
    }
    else {
        advanceButton = [[UIBarButtonItem alloc] initWithTitle: @"Simple" style:UIBarButtonItemStylePlain target: self action: @selector(hideAdvanceSearch:)];
    }
    
    self.navigationItem.rightBarButtonItem = advanceButton;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton: NO animated: YES];
    //do search
    self.currentKeyword = searchBar.text;
    self.navigationItem.rightBarButtonItem = nil;
    self.title = self.currentKeyword;
    [self fetchTwitterResults];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    [self.searchBar setShowsCancelButton: NO animated: YES];
    self.navigationItem.rightBarButtonItem = nil;
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSInteger numberOfSections = 1;
    if (self.advanceSearchEnabled) {
        numberOfSections++;
    }
    return numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == self.advanceSearchSection) {
        return 2;
    }
    else if (section == self.tweetResultsSection) {
        NSInteger numberOfRows = self.tweets.count;
        
        if (self.twitterAPI.loading || self.twitterAPI.hasMoreContent) {
            numberOfRows++;
        }
        else {
            if (self.tweets.count == 0 && self.currentKeyword) {
                return 1; //empty tableview
            }
        }
        
        return numberOfRows;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == self.advanceSearchSection) {
        if (indexPath.row == self.advanceLocationIndex) {
            Class class = [AdvanceSearchCell class];
            AdvanceSearchCell *cell = [tableView dequeueReusableCellWithIdentifier: NSStringFromClass(class) forIndexPath: indexPath];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            [cell setTitle: @"Location"];
            if (self.advanceLocation) {
                [cell setValue: [self stringFromLocation: self.advanceLocation]];
            }
            else {
                [cell setValue: @"Select location"];
            }
            return cell;
        }
        else if (indexPath.row == self.advanceUntilIndex) {
            Class class = [AdvanceSearchDatePickerCell class];
            AdvanceSearchDatePickerCell *cell = [tableView dequeueReusableCellWithIdentifier: NSStringFromClass(class) forIndexPath: indexPath];
            cell.delegate = self;
            [cell setTitle: @"Until"];
            if (self.advanceUntilDate) {
                [cell setValue: [self stringFromDate: self.advanceUntilDate]];
                cell.date = self.advanceUntilDate;
            }
            else {
                [cell setValue: @"Select until date"];
            }
            return cell;
        }
    }
    else if (indexPath.section == self.tweetResultsSection) {
        if (indexPath.row < self.tweets.count) {
            Class class = [TweetCell class];
            
            TweetCell *cell = [tableView dequeueReusableCellWithIdentifier: NSStringFromClass(class) forIndexPath: indexPath];
            cell.delegate = self;
            
            [cell configureWithTweet: self.tweets[indexPath.row]];
            return cell;
        }
        else {
            if (self.twitterAPI.loading) {
                Class class = [LoadingCell class];
                LoadingCell *cell = [tableView dequeueReusableCellWithIdentifier: NSStringFromClass(class) forIndexPath: indexPath];
                return cell;
            }
            else {
                Class class = [EmptyCell class];
                EmptyCell *cell = [tableView dequeueReusableCellWithIdentifier: NSStringFromClass(class) forIndexPath: indexPath];
                return cell;
            }
        }
    }
    
    return nil;
}

#pragma mark UITableViewDelegate

- (CGFloat) tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (void) scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.twitterAPI.hasMoreContent &&
        !self.twitterAPI.loading &&
        scrollView.contentOffset.y + 100 >  scrollView.contentSize.height - scrollView.bounds.size.height) {
        [self fetchMoreTwitterResults];
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath: indexPath animated: YES];
    if (indexPath.section == self.advanceSearchSection) {
        AdvanceSearchCell *cell = [tableView cellForRowAtIndexPath: indexPath];
        if (indexPath.row == self.advanceLocationIndex) {
            [self showMapViewController];
        }
        else if (indexPath.row == self.advanceUntilIndex) {
            if ([cell isFirstResponder]) {
                [cell resignFirstResponder];
            }
            else {
                [cell becomeFirstResponder];
            }
            
        }
    }
}

- (void) showMapViewController {
    [self.navigationController pushViewController: self.mapViewController animated: YES];
}

#pragma mark AdvanceSearchMapViewControllerDelegate

- (void) mapViewController: (AdvanceSearchMapViewController *) mapViewController pickedLocation: (CLLocationCoordinate2D) location {
    self.advanceLocation = [[CLLocation alloc] initWithLatitude: location.latitude longitude:location.longitude];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow: self.advanceLocationIndex inSection: self.advanceSearchSection];
    AdvanceSearchCell  *cell = [self.tableView cellForRowAtIndexPath: indexPath];
    [cell setValue: [self stringFromLocation: self.advanceLocation]];
}

- (NSString *) stringFromLocation: (CLLocation *) location {
    return [NSString stringWithFormat: @"%f, %f", location.coordinate.latitude, location.coordinate.longitude];
}

#pragma mark AdvanceSearchDatePickerCellDelegate

- (void) advanceSearchDatePickerCell: (AdvanceSearchDatePickerCell *) advanceSearchDatePickerCell didPickDate: (NSDate *) date {
    self.advanceUntilDate = date;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow: self.advanceUntilIndex inSection: self.advanceSearchSection];
    AdvanceSearchDatePickerCell *cell = [self.tableView cellForRowAtIndexPath: indexPath];
    NSString *dateString = [self stringFromDate: date];
    [cell setValue: dateString];
}

- (NSString *) stringFromDate: (NSDate *) date {
    static NSDateFormatter *dateFormatter;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"YYYY-MM-DD"];
        [dateFormatter setTimeZone: [NSTimeZone localTimeZone]];
    }
    return [dateFormatter stringFromDate: date];
}

#pragma mark lazy inits

- (UISearchBar *) searchBar {
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.placeholder = @"Search Twitter";
        _searchBar.delegate = self;
    }
    return _searchBar;
}

- (UITableView *) tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        Class class = [TweetCell class];
        Class loadingCellClass = [LoadingCell class];
        Class emptyCellClass = [EmptyCell class];
        Class advanceSearchCell = [AdvanceSearchCell class];
        Class advanceSearchDatePickerCell = [AdvanceSearchDatePickerCell class];
        [_tableView registerClass: class forCellReuseIdentifier: NSStringFromClass(class)];
        [_tableView registerClass: loadingCellClass forCellReuseIdentifier: NSStringFromClass(loadingCellClass)];
        [_tableView registerClass: emptyCellClass forCellReuseIdentifier: NSStringFromClass(emptyCellClass)];
        [_tableView registerClass: advanceSearchCell forCellReuseIdentifier: NSStringFromClass(advanceSearchCell)];
        [_tableView registerClass: advanceSearchDatePickerCell forCellReuseIdentifier: NSStringFromClass(advanceSearchDatePickerCell)];
    }
    return _tableView;
}

- (UIRefreshControl *) refreshControl {
    if (!_refreshControl) {
        _refreshControl = [[UIRefreshControl alloc] init];
        [_refreshControl addTarget:self action:@selector(refreshed:) forControlEvents:UIControlEventValueChanged];
    }
    return _refreshControl;
}

- (PhotoAnimator *) animator {
    if (!_animator) {
        _animator = [[PhotoAnimator alloc] init];
    }
    return _animator;
}

- (AdvanceSearchMapViewController *) mapViewController {
    if (!_mapViewController) {
        _mapViewController = [[AdvanceSearchMapViewController alloc] init];
        _mapViewController.delegate = self;
    }
    return _mapViewController;
    
}

#pragma mark TweetCellDelegate

/*
 Present a media viewer, animate from its current position
 */

- (void) tweetCellDidTapOnMedia:(TweetCell *)tweetCell {
    self.tappedMediaView = tweetCell.mediaImageView;
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell: tweetCell];
    LBTweet *tweet = self.tweets[indexPath.row];
    self.selectedTweetMedia = [tweet.medias firstObject];
    //show a full screen photo viewer
    LBTweetMedia *media = [tweet.medias firstObject];
    LBPhotoViewController *photoViewController = [[LBPhotoViewController alloc] initWithPhotoURL: [NSURL URLWithString:media.url]];
    photoViewController.transitioningDelegate = self;
    photoViewController.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController: photoViewController animated: YES completion: nil];
}

/*
 Present SafariViewController for URL
 */
- (void) tweetCell:(TweetCell *)tweetCell didTapOnURL:(NSString *)urlString {
    SFSafariViewController *safariViewController = [[SFSafariViewController alloc] initWithURL: [NSURL URLWithString: urlString]];
    [self presentViewController: safariViewController animated: YES completion:nil];
}

/*
 
 Try to open in different twitter apps when tap on a mention
 Order: Twitter, Tweetbot, Wwitterific and Safari
 
 */

- (void) tweetCell:(TweetCell *)tweetCell didTapOnMention:(NSString *)username {
    
    username = [username stringByReplacingOccurrencesOfString: @"@" withString: @""];
    
    NSURL *twitterURL = [NSURL URLWithString:[NSString stringWithFormat: @"twitter://user?screen_name=%@", username]];
    
    NSURL *twitterificURL = [NSURL URLWithString:[NSString stringWithFormat: @"twitterrific:///profile?screen_name=%@", username]];
    
    NSURL *tweetbotURL = [NSURL URLWithString:[NSString stringWithFormat: @"tweetbot:///user_profile/%@", username]];
    
    NSURL *safariURL = [NSURL URLWithString:[NSString stringWithFormat: @"https://twitter.com/%@", username]];
    
    if ([[UIApplication sharedApplication] canOpenURL: twitterURL]) {
        [[UIApplication sharedApplication] openURL: safariURL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL: tweetbotURL]) {
        [[UIApplication sharedApplication] openURL: tweetbotURL];
    }
    else if ([[UIApplication sharedApplication] canOpenURL: twitterificURL]) {
        [[UIApplication sharedApplication] openURL: twitterificURL];
    }
    else {
        [[UIApplication sharedApplication] openURL: safariURL];
    }
}

- (UIImageView *) tappedMediaView {
    return _tappedMediaView;
}

- (NSString *) tappedMediaURL {
    return self.selectedTweetMedia.url;
}

#pragma mark UIViewControllerTransitioningDelegate

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    if ([presented isKindOfClass: [LBPhotoViewController class]]) {
        PhotoAnimator *animator = [[PhotoAnimator alloc] init];
        animator.isPresenting = YES;
        animator.sourceViewController = source;
        return animator;
    }
    return nil;
}

- (nullable id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    if ([dismissed isKindOfClass: [LBPhotoViewController class]]) {
        PhotoAnimator *animator = [[PhotoAnimator alloc] init];
        animator.isPresenting = NO;
        animator.sourceViewController = self;
        return animator;
    }
    return nil;
}


@end
