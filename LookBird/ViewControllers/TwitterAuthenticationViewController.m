//
//  TwitterAuthenticationViewController.m
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "TwitterAuthenticationViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "Masonry.h"
#import "UIColor+TinyWhale.h"
@interface TwitterAuthenticationViewController ()

@property (strong, nonatomic) UIButton *authenticateButton;
@property (strong, nonatomic) UIImageView *icon;
@property (strong, nonatomic) UILabel *appNameLabel;
@property (strong, nonatomic) UILabel *authorNameLabel;
@property (strong, nonatomic) UIActivityIndicatorView *activity;

@end

@implementation TwitterAuthenticationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.

    [self.view addSubview: self.icon];
    [self.view addSubview: self.appNameLabel];
    [self.view addSubview: self.authorNameLabel];
    [self.view addSubview: self.authenticateButton];
    
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(100);
    }];
    
    [self.appNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.icon.mas_bottom).offset(20);
    }];
    
    [self.authorNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.appNameLabel.mas_bottom).offset(5);
    }];
    
    [self.authenticateButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.authorNameLabel.mas_bottom).offset(60);
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(@44);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) authenticateButtonTapped: (UIButton *) sender {
    [self showActivityIndicator];
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession * _Nullable session, NSError * _Nullable error) {
        if (session) {
            if (self.delegate && [self.delegate respondsToSelector: @selector(twitterAuthenticationViewControllerDidAuthenticate:)]) {
                [self.delegate twitterAuthenticationViewControllerDidAuthenticate: self];
            }
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self hideActivityIndicator];
            });
        }
    }];
}

- (void) showActivityIndicator {
    [self.authenticateButton setTitle: @"" forState: UIControlStateNormal];
    [self.authenticateButton addSubview: self.activity];
    [self.activity mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.authenticateButton);
    }];
    [self.activity startAnimating];
}

- (void) hideActivityIndicator {
    [self.activity removeFromSuperview];
    [self.authenticateButton setTitle: @"Please authenticate with Twitter" forState: UIControlStateNormal];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark lazy inits

- (UIButton *) authenticateButton {
    if (!_authenticateButton) {
        _authenticateButton = [UIButton buttonWithType: UIButtonTypeCustom];
        [_authenticateButton setTitle: @"Please authenticate with Twitter" forState: UIControlStateNormal];
        [_authenticateButton setTitleColor: [UIColor colorWithHexValue: 0x55acee] forState:UIControlStateNormal];
        [_authenticateButton addTarget: self action: @selector(authenticateButtonTapped:) forControlEvents: UIControlEventTouchUpInside];
        _authenticateButton.clipsToBounds = YES;
        _authenticateButton.layer.cornerRadius = 4;
        _authenticateButton.layer.borderColor = [UIColor colorWithHexValue: 0x55acee].CGColor;
        _authenticateButton.layer.borderWidth = 1;
    }
    return _authenticateButton;
}

- (UILabel *) appNameLabel {
    if (!_appNameLabel) {
        _appNameLabel = [[UILabel alloc] init];
        _appNameLabel.textColor = [UIColor blackColor];
        _appNameLabel.font = [UIFont boldSystemFontOfSize: 30];
        _appNameLabel.text = @"LookBird";
    }
    return _appNameLabel;
}

- (UILabel *) authorNameLabel {
    if (!_authorNameLabel) {
        _authorNameLabel = [[UILabel alloc] init];
        _authorNameLabel.textColor = [UIColor grayColor];
        _authorNameLabel.font = [UIFont systemFontOfSize: 20];
        _authorNameLabel.text = @"By Jason Dinh";
    }
    return _authorNameLabel;
}

- (UIImageView *) icon {
    if (!_icon) {
        _icon = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"big_icon"]];
        _icon.clipsToBounds = YES;
        _icon.layer.cornerRadius = 16;
    }
    return _icon;
}

- (UIActivityIndicatorView *) activity {
    if (!_activity) {
        _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
        [_activity startAnimating];
    }
    return _activity;
}

@end
