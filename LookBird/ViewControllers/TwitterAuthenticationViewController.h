//
//  TwitterAuthenticationViewController.h
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TwitterAuthenticationViewControllerDelegate;

@interface TwitterAuthenticationViewController : UIViewController

@property (weak) id<TwitterAuthenticationViewControllerDelegate> delegate;

@end
@protocol TwitterAuthenticationViewControllerDelegate <NSObject>
@optional
- (void) twitterAuthenticationViewControllerDidAuthenticate: (TwitterAuthenticationViewController *) twitterAuthenticationViewController;

- (void) twitterAuthenticationViewControllerFailedToAuthenticate: (TwitterAuthenticationViewController *) twitterAuthenticationViewController error: (NSError *) error;

@end