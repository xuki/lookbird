//
//  AdvanceSearchCell.m
//  LookBird
//
//  Created by Jason Dinh on 16/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "AdvanceSearchCell.h"
#import "Masonry.h"

@interface AdvanceSearchCell ()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *valueLabel;

@end

@implementation AdvanceSearchCell

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle: style reuseIdentifier: reuseIdentifier];
    if (self) {
        [self.contentView addSubview: self.titleLabel];
        [self.contentView addSubview: self.valueLabel];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.left.equalTo(self.contentView).offset(10);
        }];

        [self.valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.contentView);
            make.right.equalTo(self.contentView).offset(-10);
        }];
        
        
        [self.contentView addConstraint: [NSLayoutConstraint constraintWithItem: self.contentView
                                                                      attribute: NSLayoutAttributeHeight
                                                                      relatedBy: NSLayoutRelationEqual
                                                                         toItem: nil
                                                                      attribute: NSLayoutAttributeNotAnAttribute
                                                                     multiplier: 1.0
                                                                       constant: 50]];
    }
    return self;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    self.titleLabel.text = @"";
    self.valueLabel.text = @"";
}

- (void) setTitle: (NSString *) title {
    self.titleLabel.text = title;
}

- (void) setValue: (NSString *) value {
    self.valueLabel.text = value;
}

#pragma mark lazy inits
- (UILabel *) titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont boldSystemFontOfSize: 15.0];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.text = @"";
    }
    return _titleLabel;
}

- (UILabel *) valueLabel {
    if (!_valueLabel) {
        _valueLabel = [[UILabel alloc] init];
        _valueLabel.textColor = [UIColor grayColor];
        _valueLabel.font = [UIFont systemFontOfSize: 15.0];
        _valueLabel.text = @"";
    }
    return _valueLabel;
}

@end
