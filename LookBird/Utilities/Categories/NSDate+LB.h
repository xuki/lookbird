//
//  NSDate+LB.h
//  LookBird
//
//  Created by Jason Dinh on 13/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (LB)

- (NSString *) relativeDateString;

@end
