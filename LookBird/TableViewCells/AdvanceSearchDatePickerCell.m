//
//  AdvanceSearchDatePickerCell.m
//  LookBird
//
//  Created by Jason Dinh on 16/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "AdvanceSearchDatePickerCell.h"

@interface AdvanceSearchDatePickerCell () <UIKeyInput>

@end

@implementation AdvanceSearchDatePickerCell

//Implement UIKeyInput so this cell can show keyboard
//Then we show a UIDatePicker as keyboard
#pragma mark UIKeyInput

- (BOOL) canBecomeFirstResponder {
    return YES;
}


- (void)insertText:(NSString *)text {
    
}

- (void)deleteBackward {
    
}

- (BOOL)hasText {
    return  YES;
}

- (UIView *) inputView {
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame: CGRectZero];
    datePicker.datePickerMode = UIDatePickerModeDate;
    datePicker.maximumDate = [NSDate date];
    datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-7*24*60*60];
    if (self.date) {
        datePicker.date = self.date;
    }
    [datePicker addTarget: self action: @selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    return datePicker;
}

- (void) datePickerValueChanged: (UIDatePicker *) datePicker {
    if (self.delegate && [self.delegate respondsToSelector: @selector(advanceSearchDatePickerCell:didPickDate:)]) {
        [self.delegate advanceSearchDatePickerCell: self didPickDate: datePicker.date];
    }
}


@end
