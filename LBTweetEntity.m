//
//  LBTweetEntity.m
//  LookBird
//
//  Created by Jason Dinh on 14/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "LBTweetEntity.h"

@implementation LBTweetEntity

- (instancetype) initWithType: (LBTweetEntityType) type dictionary: (NSDictionary *) dict tweetString: (NSString *) tweetString{
    self = [super init];
    if (self) {
        self.type = type;
        switch (type) {
            case LBTweetEntityTypeHashTag:
            case LBTweetEntityTypeSymbol:
                if (dict[@"text"]) {
                    self.text = dict[@"text"];
                }
                break;
            case LBTweetEntityTypeMention:
                if (dict[@"screen_name"]) {
                    self.text = dict[@"screen_name"];
                }
                break;
            case LBTweetEntityTypeURL:
                if (dict[@"url"]) {
                    self.text = dict[@"url"];
                }
                break;
            default:
                break;
        }
    }
    
    //in case of twitter returning malformed results
    if (!self.text) {
        self.text = @"";
    }
    
    NSArray *indices = dict[@"indices"];
    NSRange range = NSMakeRange(NSNotFound, 0);
    if ([indices isKindOfClass: [NSArray class]] && indices.count == 2) {
        NSNumber *startIndex = [indices firstObject];
        NSNumber *endIndex = [indices lastObject];
        NSLog(@"start index %@ stop index %@", startIndex, endIndex);
        if ([startIndex isKindOfClass: [NSNumber class]] &&
            [endIndex isKindOfClass: [NSNumber class]]) {
            
            __block NSInteger count = 0;
            __block NSInteger realCount = 0;
            __block NSInteger startIndexInteger = [startIndex integerValue];
            __block NSInteger endIndexInteger = [endIndex integerValue];
            range = NSMakeRange(startIndexInteger, endIndexInteger - startIndexInteger);
            NSLog(@"text '%@'", [tweetString substringWithRange: range]);
            [tweetString enumerateSubstringsInRange: NSMakeRange(0, tweetString.length) options: NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString * _Nullable substring, NSRange substringRange, NSRange enclosingRange, BOOL * _Nonnull stop) {
//                NSLog(@"sub string %@ %@ %ld %ld", [tweetString substringWithRange:enclosingRange], substring, (long) count, (long) realCount);
                if (startIndexInteger == count) {
                    startIndexInteger = realCount;
                    endIndexInteger = startIndexInteger + [endIndex integerValue] - [startIndex integerValue];
                    *stop = YES;
                }
                count++;
                if (substringRange.length > 1) {
                    NSLog(@"%@ %ld", substring, substring.length);
                    realCount += substring.length;
                }
                else {
                    realCount +=1;
                }
                
                
//                NSLog(@"offset %ld", realCount - count);
            }];
            
//            NSLog(@"real start index %ld stop index %ld", (long) startIndexInteger, (long) endIndexInteger);
            
            range = NSMakeRange(startIndexInteger, endIndexInteger - startIndexInteger);
            NSLog(@"text '%@'", [tweetString substringWithRange: range]);
        }
    }
    
    if (range.location == NSNotFound) {
        NSLog(@"aaa");
        range = NSMakeRange(0, 0);
    }
    
    self.range = range;
    return self;
}

@end
