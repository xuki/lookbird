//
//  UIViewController+Good.h
//  Retro
//
//  Created by Nguyen Huy Toan on 12/8/14.
//  Copyright (c) 2014 Tiny Whale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface UIViewController (TinyWhale)

- (void) addChildVC:(UIViewController *)childVC;
- (void) removeFromParentVC;

@end
