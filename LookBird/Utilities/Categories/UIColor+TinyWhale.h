//
//  UIColor+TinyWhale.h
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (TinyWhale)

+ (UIColor *) colorWithHexValue: (NSInteger) hexValue;

@end
