//
//  TwitterAPI.h
//  LookBird
//
//  Created by Jason Dinh on 11/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <TwitterKit/TwitterKit.h>
@class LBTweet;
@interface TwitterAPI : NSObject

- (void) searchTwitterWithKeyword: (NSString *) keyword advanceParams: (NSDictionary *) advanceParams completion: (void (^)(NSArray<LBTweet *> *tweets, NSError *error)) completion;
@property BOOL loading;
@property BOOL hasMoreContent;



@end
