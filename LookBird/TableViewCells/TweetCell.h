//
//  TweetCell.h
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LBTweet;
@protocol TweetCellDelegate;
@interface TweetCell : UITableViewCell

- (void) configureWithTweet: (LBTweet *) tweet;

@property (weak) id<TweetCellDelegate> delegate;
//expose this to do fancy transition
@property (readonly, nonatomic) UIImageView *mediaImageView;

@end

@protocol TweetCellDelegate <NSObject>

- (void) tweetCellDidTapOnMedia: (TweetCell *) tweetCell;
- (void) tweetCell: (TweetCell *) tweetCell didTapOnURL: (NSString *) urlString;
- (void) tweetCell: (TweetCell *) tweetCell didTapOnMention: (NSString *) username;

@end