//
//  ViewController.m
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "ViewController.h"
#import "UIViewController+TinyWhale.h"
#import "Masonry.h"
#import "SearchViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "TwitterAuthenticationViewController.h"

@interface ViewController () <TwitterAuthenticationViewControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UINavigationController *mainNavigationController;
@property (strong, nonatomic) SearchViewController *searchViewController;
@property (strong, nonatomic) TwitterAuthenticationViewController *twitterAuthenticationViewController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
    TWTRSession *lastSession = store.session;
    
    //check login status
    if (!lastSession.authToken) {
        [self addChildVC: self.twitterAuthenticationViewController];
        [self.twitterAuthenticationViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
    else {
        [self addChildVC: self.mainNavigationController];
        [self.mainNavigationController setViewControllers: @[self.searchViewController]];
        [self.mainNavigationController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
    }
}

#pragma mark lazy inits

- (UINavigationController *) mainNavigationController {
    if (!_mainNavigationController) {
        _mainNavigationController = [[UINavigationController alloc] init];
        _mainNavigationController.view.backgroundColor = [UIColor whiteColor];
        _mainNavigationController.delegate = self;
    }
    return _mainNavigationController;
}

- (SearchViewController *) searchViewController {
    if (!_searchViewController) {
        _searchViewController = [[SearchViewController alloc] init];
    }
    return _searchViewController;
}

- (TwitterAuthenticationViewController *) twitterAuthenticationViewController {
    if (!_twitterAuthenticationViewController) {
        _twitterAuthenticationViewController = [[TwitterAuthenticationViewController alloc] init];
        _twitterAuthenticationViewController.delegate = self;
    }
    return _twitterAuthenticationViewController;
}

#pragma mark TwitterAuthenticationViewControllerDelegate

- (void) twitterAuthenticationViewControllerDidAuthenticate:(TwitterAuthenticationViewController *)twitterAuthenticationViewController {
    //animate search view controller into place
    
    [self addChildVC: self.mainNavigationController];
    [self.mainNavigationController setViewControllers: @[self.searchViewController]];
    __block MASConstraint *leftConstraint;
    [self.mainNavigationController.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.view);
        make.width.equalTo(self.view);
        leftConstraint = make.left.equalTo(self.view.mas_right);
    }];
    
    [self.view layoutIfNeeded];
    
    [leftConstraint uninstall];
    [self.mainNavigationController.view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
    }];

    
    [self.twitterAuthenticationViewController.view mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.view);
        make.width.equalTo(self.view);
        make.right.equalTo(self.view.mas_left);
    }];
    
    [UIView animateWithDuration: 0.3 delay: 0 usingSpringWithDamping:1.0 initialSpringVelocity:0.0 options:kNilOptions animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.twitterAuthenticationViewController removeFromParentVC];
    }];
}

- (void) twitterAuthenticationViewControllerFailedToAuthenticate:(TwitterAuthenticationViewController *)twitterAuthenticationViewController error:(NSError *)error {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle: @"Error" message: @"Couldn't authenticate with Twitter, please try again." preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction: [UIAlertAction actionWithTitle: @"Dismiss" style:UIAlertActionStyleDefault handler: nil]];
    [self presentViewController: alertController animated: YES completion: nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
