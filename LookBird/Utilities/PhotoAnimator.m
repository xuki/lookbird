//
//  PhotoAnimator.m
//  LookBird
//
//  Created by Jason Dinh on 14/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "PhotoAnimator.h"
#import "SearchViewController.h"
#import "LBPhotoViewController.h"
#import "UIImageView+WebCache.h"

@implementation PhotoAnimator

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.2;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    if (self.isPresenting) {
        [self animatePresentingTransition: transitionContext];
    }
    else {
        [self animateDismissingTransition: transitionContext];
    }
}

- (void) animatePresentingTransition: (id <UIViewControllerContextTransitioning>)transitionContext {
    LBPhotoViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    SearchViewController* sourceViewController = (SearchViewController *) self.sourceViewController;
    
    UIView *containerView = [transitionContext containerView];
    [containerView addSubview:toViewController.view];
    toViewController.view.alpha = 0;
    
    UIImageView *tappedMediaView = [sourceViewController tappedMediaView];
    NSString *mediaURL = [sourceViewController tappedMediaURL];
    
    UIImageView *snapshot = [[UIImageView alloc] init];
    snapshot.contentMode = UIViewContentModeScaleAspectFill;
    snapshot.clipsToBounds = YES;
    [snapshot sd_setImageWithURL: [NSURL URLWithString: mediaURL]];
    
    tappedMediaView.alpha = 0;
    
    snapshot.frame = [containerView convertRect: tappedMediaView.frame fromView:tappedMediaView.superview];
    [containerView addSubview: snapshot];
    
    CGFloat imageWidth, imageHeight, containerWidth, containerHeight;
    
    imageWidth = tappedMediaView.image.size.width;
    imageHeight = tappedMediaView.image.size.height;
    
    containerWidth = containerView.bounds.size.width;
    containerHeight = containerView.bounds.size.height;
    
    CGRect finalFrame = [self centeredRectForRatio: imageWidth/imageHeight inSize: CGSizeMake(containerWidth, containerHeight)];
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        snapshot.frame = finalFrame;
        fromViewController.view.alpha = 0;
        fromViewController.view.transform = CGAffineTransformMakeScale(0.95, 0.95);
    } completion:^(BOOL finished) {
        tappedMediaView.alpha = 1;
        toViewController.view.alpha = 1;
        fromViewController.view.alpha = 1;
        fromViewController.view.transform = CGAffineTransformIdentity;
        [snapshot removeFromSuperview];
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}

- (void) animateDismissingTransition: (id <UIViewControllerContextTransitioning>)transitionContext {
    LBPhotoViewController* fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController* toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    SearchViewController* sourceViewController = (SearchViewController *) self.sourceViewController;
    
    UIView *containerView = [transitionContext containerView];
    toViewController.view.alpha = 0;
    
    UIImageView *tappedMediaView = [sourceViewController tappedMediaView];
    
    UIImageView *snapshot = [[UIImageView alloc] init];
    snapshot.contentMode = UIViewContentModeScaleAspectFill;
    snapshot.clipsToBounds = YES;
    snapshot.image = fromViewController.imageView.image;
    
    fromViewController.imageView.alpha = 0;
    
    CGFloat imageWidth, imageHeight, containerWidth, containerHeight;
    
    imageWidth = fromViewController.imageView.image.size.width;
    imageHeight = fromViewController.imageView.image.size.height;
    
    containerWidth = containerView.bounds.size.width;
    containerHeight = containerView.bounds.size.height;
    
    CGRect beginFrame = [self centeredRectForRatio: imageWidth/imageHeight inSize: CGSizeMake(containerWidth, containerHeight)];
    
    snapshot.frame = [containerView convertRect: beginFrame fromView: fromViewController.imageView.superview];
    [containerView addSubview: snapshot];
    CGRect finalFrame = [containerView convertRect: tappedMediaView.frame fromView: tappedMediaView.superview];
    
    toViewController.view.transform = CGAffineTransformMakeScale(0.95, 0.95);
    
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        snapshot.frame = finalFrame;
        fromViewController.view.alpha = 0;
        toViewController.view.transform = CGAffineTransformIdentity;
        toViewController.view.alpha = 1;
    } completion:^(BOOL finished) {
        [snapshot removeFromSuperview];
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}


/*
 This method takes in a ratio and returns a rect that centers inside a rect with origin (0, 0) and size boundingSize
 
 eg: ratio 1 boudingSize (200, 100) -> return rect (0, 50, 100, 100) 
*/
- (CGRect) centeredRectForRatio: (CGFloat) ratio inSize: (CGSize) boundingSize {
    
    CGFloat width, height;
    
    if (ratio > boundingSize.width/boundingSize.height) {
        width = boundingSize.width;
        height = floorf(width/ratio);
    }
    else {
        height = boundingSize.height;
        width = floorf(height * ratio);
    }
    
    CGFloat left, top;
    
    left = floorf((boundingSize.width - width)/2.0);
    top = floorf((boundingSize.height - height)/2.0);
    
    CGRect rect = CGRectMake(left, top, width, height);
    return rect;
}

@end
