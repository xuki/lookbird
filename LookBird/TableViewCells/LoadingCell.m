//
//  LoadingCell.m
//  LookBird
//
//  Created by Jason Dinh on 13/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "LoadingCell.h"
#import "Masonry.h"

@interface LoadingCell ()

@property (strong, nonatomic) UIActivityIndicatorView *activity;

@end

@implementation LoadingCell

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle: style reuseIdentifier: reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview: self.activity];
        [self.activity mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.contentView);
        }];
        
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.top.left.equalTo(self);
            make.height.equalTo(@44);
        }];

    }
    return self;
}

- (void) prepareForReuse {
    [super prepareForReuse];
    [self.activity startAnimating];
}

#pragma mark lazy inits

- (UIActivityIndicatorView *) activity {
    if (!_activity) {
        _activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle: UIActivityIndicatorViewStyleGray];
        [_activity startAnimating];
    }
    return _activity;
}

@end
