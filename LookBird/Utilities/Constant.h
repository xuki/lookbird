//
//  Constant.h
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const TwitterConsumerKey;
extern NSString *const TwitterConsumerSecret;

@interface Constant : NSObject

@end
