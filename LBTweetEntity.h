//
//  LBTweetEntity.h
//  LookBird
//
//  Created by Jason Dinh on 14/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LBTweetEntityType) {
    LBTweetEntityTypeHashTag,
    LBTweetEntityTypeMention,
    LBTweetEntityTypeURL,
    LBTweetEntityTypeSymbol
};

@interface LBTweetEntity : NSObject

@property LBTweetEntityType type;
@property (copy) NSString *text;
@property NSRange range;

- (instancetype) initWithType: (LBTweetEntityType) type dictionary: (NSDictionary *) dict tweetString: (NSString *) tweetString;

@end
