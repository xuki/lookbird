//
//  TweetLabel.m
//  LookBird
//
//  Created by Jason Dinh on 16/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "TweetLabel.h"
#import "TwitterTextEntity.h"
#import "UIColor+TinyWhale.h"

@interface TweetLabel ()

@property (copy) NSArray *entities;

@end

@implementation TweetLabel

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if (self) {
        self.longPressGestureRecognizer.enabled = NO;
        NSDictionary *linkAttributes = @{(NSString *)kCTForegroundColorAttributeName: [UIColor blueColor],
                                         (NSString *)kCTUnderlineStyleAttributeName: @NO};
        NSDictionary *activeLinkAttributes = @{(NSString *)kCTForegroundColorAttributeName: [UIColor grayColor],
                                               (NSString *)kCTUnderlineStyleAttributeName: @NO};
        self.linkAttributes = linkAttributes;
        self.activeLinkAttributes = activeLinkAttributes;
    }
    return self;
}

- (TwitterTextEntity *) entityAtIndex: (NSInteger) index {
    __block TwitterTextEntity *entity;
    [self.entities enumerateObjectsUsingBlock:^(TwitterTextEntity * _Nonnull tmpEntity, NSUInteger idx, BOOL * _Nonnull stop) {
        if (NSLocationInRange(index, tmpEntity.range)) {
            entity = tmpEntity;
            *stop = YES;
        }
    }];
    return entity;
}

//highlight entity at index
- (void) setEntitySelectedAtIndex: (NSInteger) index {
    NSMutableAttributedString *tmpAttributedString = [self.attributedText mutableCopy];
    
    TwitterTextEntity *entity = [self entityAtIndex: index];
    
    if (entity) {
        [tmpAttributedString removeAttribute: NSForegroundColorAttributeName range: entity.range];
        [tmpAttributedString addAttributes: @{NSForegroundColorAttributeName: [UIColor grayColor]} range: entity.range];
        self.attributedText = tmpAttributedString;
    }
}

//de-highlight entity at index
- (void) setEntityDeselectedAtIndex: (NSInteger) index {
    NSMutableAttributedString *tmpAttributedString = [self.attributedText mutableCopy];
    
    TwitterTextEntity *entity = [self entityAtIndex: index];
    
    if (entity) {
        [tmpAttributedString removeAttribute: NSForegroundColorAttributeName range: entity.range];
        [tmpAttributedString addAttributes: @{NSForegroundColorAttributeName: [UIColor blueColor]} range: entity.range];
        self.attributedText = tmpAttributedString;
    }
}

- (void) setText:(NSString *)text withEntities:(NSArray *)entities {
    self.entities = entities;
    
    self.text = text;
    
    //add entities
    [entities enumerateObjectsUsingBlock:^(TwitterTextEntity * _Nonnull entity, NSUInteger idx, BOOL * _Nonnull stop) {
        if (entity.range.location != NSNotFound) {
            NSURL *url;
            NSString *entityText = [text substringWithRange: entity.range];
            switch (entity.type) {
                case TwitterTextEntityScreenName:
                    url = [NSURL URLWithString: [NSString stringWithFormat:@"mention://%@", entityText]];
                    break;
                case TwitterTextEntityURL:
                    url = [NSURL URLWithString: entityText];
                    break;
                case TwitterTextEntityHashtag:
                    url = [NSURL URLWithString: [NSString stringWithFormat:@"hashtag://%@", entityText]];
                    break;
                case TwitterTextEntitySymbol:
                    url = [NSURL URLWithString: [NSString stringWithFormat:@"symbol://%@", entityText]];
                    break;
                default:
                    break;
            }
            if (url) {
                [self addLinkToURL: url withRange: entity.range];
            }
        }
    }];

}

@end
