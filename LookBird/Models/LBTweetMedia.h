//
//  LBTweetMedia.h
//  LookBird
//
//  Created by Jason Dinh on 11/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//



#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LBTweetMediaType) {
    LBTweetMediaTypePhoto,
    LBTweetMediaTypeVideo,
    LBTweetMediaTypeUnknown
};

@interface LBTweetMedia : NSObject

@property (copy) NSString *mediaID;
@property (copy) NSString *url;
@property LBTweetMediaType type;

- (instancetype) initWithJSONDictionary: (NSDictionary *) dictionary;

@end
