//
//  TweetCell.m
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "TweetCell.h"
#import "Masonry.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <TwitterKit/TwitterKit.h>
#import "LBTweet.h"
#import "LBTweetMedia.h"
#import "NSDate+LB.h"
#import "TwitterTextEntity.h"
#import "TweetLabel.h"


//let's copy tweetbot because why not

@interface TweetCell () <TTTAttributedLabelDelegate>

@property (strong, nonatomic) UIImageView *avatar;
@property (strong, nonatomic) UILabel *handleLabel;
@property (strong, nonatomic) TweetLabel *tweetLabel;
@property (strong, nonatomic) UILabel *timeStampLabel;
@property (strong, nonatomic) UIImageView *mediaImageView;
@property (strong) LBTweet *tweet;

@property (strong) MASConstraint *mediaWidthConstraint;
@property (strong) MASConstraint *mediaHeightConstraint;
@end

@implementation TweetCell

- (instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle: style reuseIdentifier: reuseIdentifier];
    if (self) {
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self);
        }];
        
        [self.contentView addSubview: self.avatar];
        [self.contentView addSubview: self.handleLabel];
        [self.contentView addSubview: self.tweetLabel];
        [self.contentView addSubview: self.timeStampLabel];
        [self.contentView addSubview: self.mediaImageView];
        
        //layout
        
        [self.avatar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.contentView).offset(10);
            make.left.equalTo(self.contentView).offset(10);
            make.width.equalTo(@40);
            make.height.equalTo(@40);
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-10);
        }];
        
        [self.handleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.avatar);
            make.left.equalTo(self.avatar.mas_right).offset(10);
        }];
        
        [self.tweetLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.handleLabel);
            make.top.equalTo(self.handleLabel.mas_bottom).offset(10);
            make.right.equalTo(self.mediaImageView.mas_left).offset(-10);
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-10);
        }];
        
        [self.timeStampLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-10);
            make.top.equalTo(self.handleLabel);
        }];
        
        //size 0 0 because we don't know if we have a media
        [self.mediaImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.timeStampLabel.mas_bottom).offset(10);
            make.right.equalTo(self.timeStampLabel);
            self.mediaWidthConstraint = make.width.equalTo(@0);
            self.mediaHeightConstraint = make.height.equalTo(@0);
            make.bottom.lessThanOrEqualTo(self.contentView).offset(-10);
        }];
        
        //gesture recognizers for media tap and entity tap (longpress)
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action: @selector(mediaTapped:)];
        [self.mediaImageView addGestureRecognizer: tap];
    }
    return self;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void) mediaTapped: (UITapGestureRecognizer *) gesture {
    if (!self.mediaImageView.image) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector: @selector(tweetCellDidTapOnMedia:)]) {
        [self.delegate tweetCellDidTapOnMedia: self];
    }
}

- (void) prepareForReuse {
    [super prepareForReuse];
    [self.mediaHeightConstraint uninstall];
    [self.mediaWidthConstraint uninstall];
    [self.mediaImageView mas_updateConstraints:^(MASConstraintMaker *make) {
        self.mediaWidthConstraint = make.width.equalTo(@0);
        self.mediaHeightConstraint = make.height.equalTo(@0);
    }];
}

//configure the cell with tweet info
//need to retain the tweet to handle touch events
//not sure if this view is the right place to handle touch events though
- (void) configureWithTweet: (LBTweet *) tweet {
    self.tweet = tweet;
    
    [self.avatar sd_setImageWithURL: [NSURL URLWithString: tweet.author.profileImageLargeURL] placeholderImage: nil];
    self.handleLabel.text = [NSString stringWithFormat: @"@%@", tweet.author.screenName];
    [self.tweetLabel setText: tweet.text withEntities: tweet.entities];
    self.timeStampLabel.text = [tweet.createdAt relativeDateString];
    
    if (tweet.medias.count) {
        LBTweetMedia *media = [tweet.medias firstObject];
        
        //TODO: support video and gif
        [self.mediaHeightConstraint uninstall];
        [self.mediaWidthConstraint uninstall];
        [self.mediaImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            self.mediaWidthConstraint = make.width.equalTo(@70);
            self.mediaHeightConstraint = make.height.equalTo(@70);
        }];
        [self.mediaImageView sd_setImageWithURL: [NSURL URLWithString: media.url]];
    }
}

#pragma mark lazy inits

- (UIImageView *) avatar {
    if (!_avatar) {
        _avatar = [[UIImageView alloc] init];
        _avatar.backgroundColor = [UIColor lightGrayColor];
        _avatar.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _avatar;
}

- (UIImageView *) mediaImageView {
    if (!_mediaImageView) {
        _mediaImageView = [[UIImageView alloc] init];
        _mediaImageView.userInteractionEnabled = YES;
        _mediaImageView.backgroundColor = [UIColor lightGrayColor];
        _mediaImageView.contentMode = UIViewContentModeScaleAspectFill;
        _mediaImageView.clipsToBounds = YES;
    }
    return _mediaImageView;
}

- (UILabel *) handleLabel {
    if (!_handleLabel) {
        _handleLabel = [[UILabel alloc] init];
        _handleLabel.textColor = [UIColor blackColor];
        _handleLabel.font = [UIFont systemFontOfSize: 15.0];
        [_handleLabel setContentCompressionResistancePriority: UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisHorizontal];
    }
    return _handleLabel;
}

- (TweetLabel *) tweetLabel {
    if (!_tweetLabel) {
        _tweetLabel = [[TweetLabel alloc] initWithFrame: CGRectZero];
        _tweetLabel.delegate = self;
        _tweetLabel.userInteractionEnabled = YES;
        _tweetLabel.numberOfLines = 0;
        _tweetLabel.textColor = [UIColor blackColor];
    }
    return _tweetLabel;
}

- (UILabel *) timeStampLabel {
    if (!_timeStampLabel) {
        _timeStampLabel = [[UILabel alloc] init];
        _timeStampLabel.textColor = [UIColor lightGrayColor];
        _timeStampLabel.font = [UIFont systemFontOfSize: 15.0];
    }
    return _timeStampLabel;
}

#pragma mark TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    NSString *scheme = url.scheme;
    if ([scheme isEqualToString: @"mention"]) {
        NSString *username = [url.absoluteString stringByReplacingOccurrencesOfString: @"mention://@" withString: @""];
        if (self.delegate && [self.delegate respondsToSelector: @selector(tweetCell:didTapOnMention:)]) {
            [self.delegate tweetCell: self didTapOnMention: username];
        }
    }
    //we're not handling hashtag and symbol right now, exclude them. The rest are URLs
    else if ([scheme isEqualToString: @"hashtag"] || [scheme isEqualToString: @"symbol"]) {
        NSLog(@"not handling %@ right now", [url absoluteString]);
    }
    else {
        if (self.delegate && [self.delegate respondsToSelector: @selector(tweetCell:didTapOnURL:)]) {
            [self.delegate tweetCell: self didTapOnURL: url.absoluteString];
        }
    }
}

@end
