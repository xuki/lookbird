//
//  UIViewController+Good.m
//  Retro
//
//  Created by Nguyen Huy Toan on 12/8/14.
//  Copyright (c) 2014 Tiny Whale. All rights reserved.
//

#import "UIViewController+TinyWhale.h"

@implementation UIViewController (TinyWhale)

- (void)addChildVC:(UIViewController *)childVC{
    [self addChildViewController:childVC];
    [self.view addSubview:childVC.view];
    [childVC didMoveToParentViewController:self];
}

- (void)removeFromParentVC{
    [self willMoveToParentViewController:nil];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

@end
