//
//  NSDate+LB.m
//  LookBird
//
//  Created by Jason Dinh on 13/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "NSDate+LB.h"

@implementation NSDate (LB)

- (NSString *) relativeDateString {
    NSUInteger timeInterval = -[self timeIntervalSinceNow];
    
    if (timeInterval < 60) {
        return [NSString stringWithFormat: @"%lds", (long) timeInterval];
    }
    else if (timeInterval < 60 * 60) {
        NSInteger minutes = timeInterval / 60;
        return [NSString stringWithFormat: @"%ldm", (long) minutes];
    }
    else if (timeInterval < 24 * 60 * 60) {
        NSInteger hours = timeInterval / (60 * 60);
        return [NSString stringWithFormat: @"%ldh", (long) hours];
    }
    else {
        NSInteger days = timeInterval / (24 * 60 * 60);
        return [NSString stringWithFormat: @"%ldd", (long) days];
    }
    
    return @"";
}

@end
