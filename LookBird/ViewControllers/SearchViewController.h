//
//  SearchViewController.h
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController

- (UIImageView *) tappedMediaView;
- (NSString *) tappedMediaURL;

@end
