//
//  LBTweet.h
//  LookBird
//
//  Created by Jason Dinh on 11/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <TwitterKit/TwitterKit.h>
@class LBTweetMedia;
@class TwitterTextEntity;
@interface LBTweet : TWTRTweet

@property (copy) NSArray<LBTweetMedia *> *medias;
@property (copy) NSArray<TwitterTextEntity *> *entities;
@end
