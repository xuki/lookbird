//
//  LBTweet.m
//  LookBird
//
//  Created by Jason Dinh on 11/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "LBTweet.h"
#import "LBTweetMedia.h"
#import "TwitterText.h"

@implementation LBTweet

- (instancetype)initWithJSONDictionary:(NSDictionary *)dictionary {  
    self = [super initWithJSONDictionary: dictionary];
    if ([dictionary[@"entities"] isKindOfClass:[NSDictionary class]]) {
        NSDictionary *entitiesDict = dictionary[@"entities"];
        if ([entitiesDict[@"media"] isKindOfClass: [NSArray class]] && [entitiesDict[@"media"] count] > 0) {
            NSDictionary *mediaDict = [entitiesDict[@"media"] firstObject];
            LBTweetMedia *media = [[LBTweetMedia alloc] initWithJSONDictionary: mediaDict];
            self.medias = @[media];
        }
        NSArray *entities = [TwitterText entitiesInText: self.text];
        self.entities = entities;
    }
    return self;
}

+ (NSArray *)tweetsWithJSONArray:(NSArray *)array {
    return [super tweetsWithJSONArray: array];
}

@end
