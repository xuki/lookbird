//
//  UIColor+TinyWhale.m
//  LookBird
//
//  Created by Jason Dinh on 10/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "UIColor+TinyWhale.h"

@implementation UIColor (TinyWhale)

+ (UIColor *) colorWithHexValue: (NSInteger) hexValue {
    return [UIColor colorWithRed:((CGFloat)((hexValue & 0xFF0000) >> 16))/255.0 green:((CGFloat)((hexValue & 0xFF00) >> 8))/255.0 blue:((CGFloat)(hexValue & 0xFF))/255.0 alpha:1.0];
}

@end
