//
//  AdvanceSearchCell.h
//  LookBird
//
//  Created by Jason Dinh on 16/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdvanceSearchCell : UITableViewCell

- (void) setTitle: (NSString *) title;
- (void) setValue: (NSString *) value;

@end
