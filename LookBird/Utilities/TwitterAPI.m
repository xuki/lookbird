//
//  TwitterAPI.m
//  LookBird
//
//  Created by Jason Dinh on 11/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "TwitterAPI.h"
#import "LBTweet.h"


@interface TwitterAPI ()

@property (strong, nonatomic) TWTRAPIClient *client;
@property (copy) NSString *nextURL;

@end
@implementation TwitterAPI

- (void) searchTwitterWithKeyword: (NSString *) keyword advanceParams: (NSDictionary *) advanceParams completion: (void (^)(NSArray<LBTweet *> *tweets, NSError *error)) completion {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary: @{@"q": keyword,
                                                                                   @"result_type": @"recent",
                                                                                   @"count": @"50",
                                                                                   @"include_entities": @"true"}];
    if (advanceParams) {
        [params addEntriesFromDictionary: advanceParams];
    }
    
    [self searchTwitterWithParams: [params copy] completion: completion];
}

- (void) searchTwitterWithParams: (NSDictionary *) params completion: (void (^)(NSArray<LBTweet *> *tweets, NSError *error)) completion {
    static NSString *searchEndpoint = @"https://api.twitter.com/1.1/search/tweets.json";
    self.loading = YES;
    
    NSString *url;
    if (self.nextURL) {
        url = self.nextURL;
    }
    else {
        url = searchEndpoint;
    }
    
    NSError *error;
    NSURLRequest *request = [self.client URLRequestWithMethod:@"GET" URL: url parameters: params error: &error];
    if (request) {
        [self.client sendTwitterRequest: request completion:^(NSURLResponse * _Nullable response, NSData * _Nullable data, NSError * _Nullable connectionError) {
            
            if (data) {
                //dispatch to background to avoid work on main thread
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSError *jsonError;
                    NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData: data
                                                                                 options:kNilOptions
                                                                                   error: &jsonError];
                    if (responseDict) {
                        
                        if (responseDict[@"search_metadata"] &&
                            [responseDict[@"search_metadata"] isKindOfClass:[NSDictionary class]] &&
                            responseDict[@"search_metadata"][@"next_results"]) {
                            self.hasMoreContent = YES;
                            self.nextURL = [searchEndpoint stringByAppendingString: responseDict[@"search_metadata"][@"next_results"]];
                        }
                        else {
                            self.hasMoreContent = NO;
                        }
                        
                        NSMutableArray *tweets = [NSMutableArray array];
                        
                        NSArray<NSDictionary *> *statusDict = responseDict[@"statuses"];
                        
                        [statusDict enumerateObjectsUsingBlock:^(NSDictionary * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                            LBTweet *tweet = [[LBTweet alloc] initWithJSONDictionary: obj];
                            [tweets addObject:tweet];
                        }];
                        
                        completion(tweets, nil);
                    }
                    else {
                        NSLog(@"JSON parse error: %@", jsonError);
                        completion(nil, error);
                    }
                    self.loading = NO;
                });
            }
            else {
                NSLog(@"Twitter search error: %@", connectionError);
                completion(nil, connectionError);
                self.loading = NO;
            }
            
        }];
    }
    else {
        NSLog(@"NSURLRequest creation error: %@", error);
        self.loading = NO;
    }
}

#pragma mark lazy inits

- (TWTRAPIClient *) client {
    if (!_client) {
        TWTRSessionStore *store = [[Twitter sharedInstance] sessionStore];
        TWTRSession *lastSession = store.session;
        _client = [[TWTRAPIClient alloc] initWithUserID: lastSession.userID];
    }
    return _client;
}

@end
