//
//  AdvanceSearchMapViewController.h
//  LookBird
//
//  Created by Jason Dinh on 16/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol AdvanceSearchMapViewControllerDelegate;
@import MapKit;
@interface AdvanceSearchMapViewController : UIViewController

@property (weak) id<AdvanceSearchMapViewControllerDelegate> delegate;
@property CLLocation *location;

@end

@protocol AdvanceSearchMapViewControllerDelegate <NSObject>

- (void) mapViewController: (AdvanceSearchMapViewController *) mapViewController pickedLocation: (CLLocationCoordinate2D) location;

@end