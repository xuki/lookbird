//
//  AdvanceSearchDatePickerCell.h
//  LookBird
//
//  Created by Jason Dinh on 16/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "AdvanceSearchCell.h"
@protocol AdvanceSearchDatePickerCellDelegate;
@interface AdvanceSearchDatePickerCell : AdvanceSearchCell

//the default date for the datepicker
@property (strong) NSDate *date;
@property (weak) id<AdvanceSearchDatePickerCellDelegate> delegate;

@end

@protocol AdvanceSearchDatePickerCellDelegate <NSObject>

- (void) advanceSearchDatePickerCell: (AdvanceSearchDatePickerCell *) advanceSearchDatePickerCell didPickDate: (NSDate *) date;

@end