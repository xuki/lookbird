//
//  AdvanceSearchMapViewController.m
//  LookBird
//
//  Created by Jason Dinh on 16/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "AdvanceSearchMapViewController.h"
#import "Masonry.h"

@interface AdvanceSearchMapViewController ()

@property (strong, nonatomic) MKMapView *mapView;
@property (strong, nonatomic) UIImageView *pin;

@end
@implementation AdvanceSearchMapViewController

- (void) viewDidLoad {
    [super viewDidLoad];
    self.title = @"Select location";
    [self.view addSubview: self.mapView];
    [self.view addSubview: self.pin];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    //pin need to be offset height/2 for accuracy
    [self.pin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.mapView);
        make.centerY.equalTo(self.mapView).offset(-self.pin.bounds.size.height/2);
    }];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    if (self.location) {
        self.mapView.centerCoordinate = self.location.coordinate;
    }
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    if (self.delegate && [self.delegate respondsToSelector: @selector(mapViewController:pickedLocation:)]) {
        [self.delegate mapViewController: self pickedLocation: self.mapView.centerCoordinate];
    }
}

#pragma mark lazy inits

- (MKMapView *) mapView {
    if (!_mapView) {
        _mapView = [[MKMapView alloc] init];
    }
    return _mapView;
}

- (UIImageView *) pin {
    if (!_pin) {
        _pin = [[UIImageView alloc] initWithImage: [UIImage imageNamed: @"pin"]];
    }
    return _pin;
}


@end
