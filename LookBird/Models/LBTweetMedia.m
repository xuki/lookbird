//
//  LBTweetMedia.m
//  LookBird
//
//  Created by Jason Dinh on 11/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import "LBTweetMedia.h"

@implementation LBTweetMedia

- (instancetype) initWithJSONDictionary: (NSDictionary *) dictionary {
    self = [super init];
    if (self) {
        self.url = dictionary[@"media_url_https"];
        NSString *type = dictionary[@"type"];
        //TODO: detect GIF
        if ([type isEqualToString:@"photo"]) {
            self.type = LBTweetMediaTypePhoto;
        }
        else if ([type isEqualToString:@"video"]) {
            self.type = LBTweetMediaTypeVideo;
        }
        else {
            self.type = LBTweetMediaTypeUnknown;
        }
    }
    return self;
}

@end
