# LookBird

LookBird is a simple application to search on Twitter.

###Build requirements:

* Xcode 7 or newer.
* iOS 9.0 or newer. This application should work on iOS 8.0 but not tested.
* CocoaPods 0.3.9 or newer.

###How to build:

* Run `pod install` at the top directory.
* Open `LookBird.xcworkspace`, update the bundle identifier and provisioning profile.
* Build using Xcode.

###Features:

* Authenticate with Twitter.
* Search Twitter by keyword, location and date.
* Pagination support.
* Handle embedded photo, mention and url.

###Open source:

LookBird makes use of the following libaries:

* [TwitterKit](https://fabric.io/kits/ios/twitterkit): handle Twitter authentication and API requests.
* [twitter-text](https://github.com/twitter/twitter-text): to parse tweet entities.
* [Masonry](https://github.com/SnapKit/Masonry): an Objective-C AutoLayout wrapper.
* [SDWebImage](https://github.com/rs/SDWebImage): load and display image asynchronous.
* [TTTAttributedLabel](https://github.com/TTTAttributedLabel/TTTAttributedLabel): display attributed string.


###Known issues:

* Does not handle videos/GIFs properly. Will show as photo instead.


###Time spent on this application: ~20 hours