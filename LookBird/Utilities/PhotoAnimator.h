//
//  PhotoAnimator.h
//  LookBird
//
//  Created by Jason Dinh on 14/1/16.
//  Copyright © 2016 Tiny Whale. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;
@interface PhotoAnimator : NSObject <UIViewControllerAnimatedTransitioning>

@property (weak) UIViewController *sourceViewController;

//being presented = YES, dismissed = NO
@property BOOL isPresenting;
@end
